@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')

        <div class="fix content_area">
            <div class="duties_author_ijirk_text fix">
                <h1 style="text-align: center;margin-top: 10px;"> DUTIES OF AUTHORS</h1><br/>

                <h4>ADHERENCE TO PEER REVIEW & PUBLICATION CONVENTIONS</h4><br/>
                <p>•Authors should not submit paper that is already submitted or published in any other journal.
                </p>
                <p>•	Submitting the same manuscript to more than one journal concurrently constitutes unethical publishing behavior and is unacceptable.
                </p>
                <p>•If the research work is continuation of the previous work that is already published, then a clear and complete reference to the published paper must be included in the reference section.
                </p>
                <p>•	Author can use maximum of 30% of the previously published research work in their paper to support the idea.
                </p>
                <p>•Authors should respond to reviewers’ comments in a professional and timely manner.
                </p>
                <p>•All authors should agree to pay Article Processing Charges to publish their paper.
                </p>
                <p>•	Authors should follow withdrawal policies if they wish to withdraw their paper during the peer-review process or after acceptance.
                </p>

               
                <h3>Transparency</h3>
                <p>•Sources of funding for research or publication should always be disclosed.</p>
                <p>•	If any assistance, help or guidance is taken as support for publications, it should be clearly mentioned in the manuscript under acknowledgement section.</p>
                <p>•Authors should disclose relevant financial and non-financial interests and relationships that might be considered likely to affect the interpretation of their findings.</p>
                <p>•Authors should follow journal and institutional requirements for disclosing competing interests.</p>
              

                <h3>Acknowledgement of Sources</h3>
                <p>•Author should disclose all sources that support the research and publication of the submitted work. This includes direct and indirect funding source, supply of equipment or materials.</p>
             
                <h3>Authorship & Acknowledgement</h3>
                <p>•The list of authors should accurately reflect who did the work. All submitted work should be attributed to one or more authors.</p>
                <p>•	Author should provide a complete list of manuscript authors (names and affiliations) who did the work for scholarly publication. The information must be provided at the time of submission.</p>
                <p>•	TThere should not be ghost or guest authors in the manuscript.</p>
                <p>•	Author must give proper acknowledgment of the work of others.</p>
                <p>•	Those who made less substantial contributions to the research for example reviewing, proof reading, supervisors, head of department etc., their names should be listed in an acknowledgement section instead of declaring them as manuscript author.</p>
        

                <h3>Originality & Plagiarism</h3>
                <p>•	Authors should ensure that they have written entirely original work, and if authors have used the work and/or words of others, that this has been appropriately cited or quoted.
                </p>
                <p>•	Authors should declare that the work reported is their own and that they are the copyright owner.</p>
                <p>•Authors should seek permission if they are using images, videos, tables and other information from other sources.</p>
                <p>•	Plagiarism takes many forms, from substantial copying, paraphrasing, text-recycling to manipulation of images, data etc. Authors should understand that plagiarism in all forms constitutes unethical publishing behavior and is unacceptable.</p>
         

                <h3>Respecting Cultures and Heritage</h3>
                <p>•Author should ensure that the content of their paper does not harm cultural and heritage values of societies. Images, videos and other content should be carefully used to avoid disputes.</p>
               
            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>


</html>