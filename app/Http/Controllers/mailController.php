<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
class mailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('mail');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   /* public function post(Request $request)
    {

       $data=[
         'name'=>$request->name,
         'email'=>$request->email,
         'details'=>$request->details,
         'name'=>$request->name,
       ];
       Mail::send('mail.mail',$data,function($message) use ($data){
return $message->from('nbiplob15@gmail.com','biplobnbiplob15@gmail.com');
$message->to($data['email']);
$message->details($data['details']);

       });
       return redirect()->back();
    }*/


    public function post(Request $request)
    {

        Mail::send('mail.mail', [], function ($m) use($request){
            $m
                ->from($request->get('email'))
                ->to('nbiplob152@gmail.com')
                ->subject('Your Reminder!');
        });

        return 'Email Sent';

    }
    /* public function post(Request $request)
     {

       Mail::send('mail.mail', [], function ($m) use($request){
            $m
                 ->from($request->get('email'))
                 ->to('nbiplob152@gmail.com')
                ;
         });

         return 'Email Sent';

     }*/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
